package com.r1chjames.iot.monitoring.services;

import com.r1chjames.iot.monitoring.entities.Task;
import org.quartz.CronScheduleBuilder;
import org.quartz.JobDataMap;
import org.quartz.JobDetail;
import org.quartz.Trigger;

import java.util.TimeZone;

import static org.quartz.JobBuilder.newJob;
import static org.quartz.TriggerBuilder.newTrigger;

public class SchedulerFactory {
    private SchedulerFactory() {
    }

    private static final String WEBHOOK_TASK_PARAM = "task";

//    public static JobDetail job(Task task) {
//        final JobDataMap data = new JobDataMap();
//        put(data, task);
//        return newJob(WebhookJob.class).usingJobData(data).build();
//    }

    public static void put(JobDataMap map, Task task) {
        map.put(WEBHOOK_TASK_PARAM, task);
    }

    public static Task from(JobDataMap dataMap) {
        return (Task) dataMap.get(WEBHOOK_TASK_PARAM);
    }

    public static Trigger cronTrigger(String cronSchedule) {
        return newTrigger().withSchedule(CronScheduleBuilder.cronSchedule(cronSchedule)
            .inTimeZone(TimeZone.getTimeZone("Europe/London"))
        ).build();
    }
}
