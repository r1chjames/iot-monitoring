package com.r1chjames.iot.monitoring.services;

import com.r1chjames.iot.monitoring.entities.Task;

import javax.inject.Singleton;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

@Singleton
public class TaskService {
    private final Map<UUID, Task> tasks;

    public TaskService() {
        tasks = new HashMap<>();
    }

    public UUID add(Task task) {
        tasks.put(task.getId(), task);
        return task.getId();
    }

    public Collection<Task> findAll() {
        return tasks.values();
    }

    public void update(Task task) {
        tasks.put(task.getId(), task);
    }

    public void delete(UUID id) {
        tasks.remove(id);
    }
}
