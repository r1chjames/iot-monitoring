package com.r1chjames.iot.monitoring.services;

import com.r1chjames.iot.monitoring.dao.DeviceDao;
import com.r1chjames.iot.monitoring.entities.Device;
import org.jdbi.v3.core.Jdbi;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
public class DeviceService {

    private DeviceDao deviceDao;

    @Autowired
    public void deviceDao(Jdbi dbi) {
        deviceDao = dbi.onDemand(DeviceDao.class);
    }

    public Device getDeviceById(UUID id) {
        return deviceDao.getDeviceById(id);
    }

    public Device getDeviceByIp(String ip) {
        return deviceDao.getDeviceByIp(ip);
    }

    public Device addDevice(Device device) {
        return deviceDao.addDevice(device);
    }

    public Device fullUpdateDevice(UUID id, Device updatedDevice) {
        return deviceDao.updateDevice(id, updatedDevice);
    }

    public Device partialUdateDevice(UUID id, Device updatedDevice) {
        return deviceDao.updateDevice(id, updatedDevice);
    }

    public void daleteDevice(UUID id) {
        deviceDao.deleteDevice(id);
    }
}
