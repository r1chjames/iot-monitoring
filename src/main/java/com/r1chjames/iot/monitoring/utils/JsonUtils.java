package com.r1chjames.iot.monitoring.utils;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.r1chjames.iot.monitoring.exception.ApplicationException;
import lombok.extern.slf4j.Slf4j;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.Optional;

import static com.r1chjames.iot.monitoring.exception.ErrorSequences.JSON_PARSING_ERROR;
import static com.r1chjames.iot.monitoring.exception.ErrorSequences.JSON_PARSING_ERROR_RETURNING_EMPTY;

@Slf4j
public class JsonUtils {

    public static ObjectMapper mapper = new ObjectMapper();

    static {
        mapper.registerModule(new JavaTimeModule());
        mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        mapper.configure(DeserializationFeature.FAIL_ON_INVALID_SUBTYPE, false);
        mapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);
    }

    public static void writeJsonFile(String path, Object value) throws IOException {
        mapper.writerWithDefaultPrettyPrinter().writeValue(new File(path), value);
    }

    public static String toJson(Object value) {
        try {
            return mapper.writerWithDefaultPrettyPrinter().writeValueAsString(value);
        } catch (JsonProcessingException e) {
            log.error(JSON_PARSING_ERROR.getMessage(), e);
            throw new ApplicationException(JSON_PARSING_ERROR);
        }
    }

    public static <T> Optional<T> fromJson(String json, Class<T> type) {
        try {
            return Optional.of(mapper.readValue(json, type));
        } catch (Exception e) {
            log.error(JSON_PARSING_ERROR_RETURNING_EMPTY.getMessage());
            return Optional.empty();
        }
    }

    public static <T> Optional<List<T>> fromJsonList(String json, Class<T> type) {
        try {
            return Optional.of(mapper.readValue(json, mapper.getTypeFactory().constructCollectionType(List.class, type)));
        } catch (IOException e) {
            log.error(JSON_PARSING_ERROR_RETURNING_EMPTY.getMessage(), e);
            return Optional.empty();

        }
    }

    public static <T> T readJsonFile(String fileName, Class<T> type) throws IOException {
        return mapper.readValue(readFile(fileName), type);
    }

    public static <T> T readJsonFile(File file, Class<T> type) throws IOException {
        return mapper.readValue(file, type);
    }

    public static String readFile(String path) throws IOException {
        if (!path.equals("")) {
            return new String(Files.readAllBytes(Paths.get(path)));
        }
        return null;
    }

}

