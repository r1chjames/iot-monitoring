package com.r1chjames.iot.monitoring.entities;

import java.time.LocalDateTime;
import java.util.UUID;

public class DeviceStatus {

    private UUID id;
    private UUID deviceId;
    private LocalDateTime lastPolled;
    private DeviceStatusEnum status;
}
