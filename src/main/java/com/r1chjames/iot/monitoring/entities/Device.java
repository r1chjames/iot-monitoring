package com.r1chjames.iot.monitoring.entities;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;
import java.util.UUID;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Device {

    private UUID id;
    private String name;
    private String type;
    private String ip;
    private LocalDateTime lastUpdated;
    private MonitorType monitorType;

}
