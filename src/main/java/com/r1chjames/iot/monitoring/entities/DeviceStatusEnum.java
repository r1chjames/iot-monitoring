package com.r1chjames.iot.monitoring.entities;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public enum DeviceStatusEnum {

    ONLINE("ONLINE"),
    OFFILINE("OFFLINE");

    private String status;

}
