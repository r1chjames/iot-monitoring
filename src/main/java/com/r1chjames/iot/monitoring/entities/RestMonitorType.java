package com.r1chjames.iot.monitoring.entities;

import java.util.Map;

public class RestMonitorType {

    private String uri;
    private String method;
    private String body;
    private String contentType;
    private Map<String, String> headers;
}
