package com.r1chjames.iot.monitoring.entities;

import com.cronutils.descriptor.CronDescriptor;
import com.cronutils.model.CronType;
import com.cronutils.model.definition.CronDefinitionBuilder;
import com.cronutils.parser.CronParser;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;
import java.util.Locale;
import java.util.UUID;

import static com.cronutils.utils.Preconditions.checkState;

@Getter
@Setter
@AllArgsConstructor
public class Task {
    private static final CronDescriptor descriptor = CronDescriptor.instance(Locale.UK);
    private static final CronParser parser = new CronParser(CronDefinitionBuilder.instanceDefinitionFor(CronType.QUARTZ));

    private UUID id;
    private Device device;
    private String cronExpression;
    private LocalDateTime lastUpdated;

    public Task validate() {
        checkState(parser.parse(cronExpression) != null, "Bad cron expression " + cronExpression);
        return this;
    }

    public String getReadableSchedule() {
        return descriptor.describe(parser.parse(cronExpression));
    }



}
