package com.r1chjames.iot.monitoring;

import org.jdbi.v3.core.Jdbi;
import org.jdbi.v3.sqlobject.SqlObjectFactory;
import org.jdbi.v3.sqlobject.SqlObjectPlugin;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import javax.sql.DataSource;


@SpringBootApplication
public class Application {

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

    @Autowired
    @Bean
    public Jdbi dbi(DataSource dataSource) {
        synchronized (Jdbi.class) {
            return Jdbi.create(dataSource)
                    .installPlugin(new SqlObjectPlugin());
        }
    }
}
