package com.r1chjames.iot.monitoring.dao.mappers;

import com.r1chjames.iot.monitoring.entities.Device;
import com.r1chjames.iot.monitoring.entities.Task;
import org.jdbi.v3.core.mapper.RowMapper;
import org.jdbi.v3.core.statement.StatementContext;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.UUID;

public class TaskMapper implements RowMapper<Task> {
    @Override
    public Task map(ResultSet rs, StatementContext ctx) throws SQLException {

        Device device = new DeviceMapper().map(rs, ctx);

        return new Task(UUID.fromString(rs.getString("id")),
                device,
                rs.getString("cron_expression"),
                rs.getTimestamp("last_updated").toLocalDateTime()
        );
    }
}
