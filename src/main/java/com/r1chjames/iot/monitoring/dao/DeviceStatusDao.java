package com.r1chjames.iot.monitoring.dao;

import com.r1chjames.iot.monitoring.entities.DeviceStatus;
import org.jdbi.v3.sqlobject.customizer.Bind;
import org.jdbi.v3.sqlobject.customizer.BindBean;
import org.jdbi.v3.sqlobject.statement.SqlQuery;
import org.jdbi.v3.sqlobject.statement.SqlUpdate;

import java.util.UUID;

public interface DeviceStatusDao {

    String allFields = "id, device_id, last_polled, status ";
    String updateFields = ":status.id, :status.deviceId, :status.lastPolled, :status.status ";
    String tableName = "device_status ";

    @SqlQuery(
            "SELECT " +
                    allFields +
                    " FROM "
                    + tableName +
                    "WHERE " +
                    "device_id = :deviceId")
    DeviceStatus getDeviceStatusByDeviceId(@Bind("deviceId") UUID device_id);

    @SqlUpdate(
            "INSERT INTO " +
                    tableName +
                    "(" + allFields + ")" +
                    "VALUES " +
                    allFields)
    void updateDeviceStatus(@BindBean("status") DeviceStatus status);
}
