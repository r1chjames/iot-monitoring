package com.r1chjames.iot.monitoring.dao.mappers;

import com.r1chjames.iot.monitoring.entities.Device;
import com.r1chjames.iot.monitoring.entities.MonitorType;
import org.jdbi.v3.core.mapper.RowMapper;
import org.jdbi.v3.core.statement.StatementContext;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.UUID;

import static com.r1chjames.iot.monitoring.utils.JsonUtils.fromJson;

public class DeviceMapper implements RowMapper<Device> {
    @Override
    public Device map(ResultSet rs, StatementContext ctx) throws SQLException {
        return new Device(UUID.fromString(rs.getString("d.id")),
                rs.getString("d.name"),
                rs.getString("d.type"),
                rs.getString("d.ip"),
                rs.getTimestamp("d.last_updated").toLocalDateTime(),
                fromJson(rs.getString("d.monitor_type"), MonitorType.class).orElse(null)
        );
    }
}
