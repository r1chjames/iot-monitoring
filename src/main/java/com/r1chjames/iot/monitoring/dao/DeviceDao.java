package com.r1chjames.iot.monitoring.dao;

import com.r1chjames.iot.monitoring.dao.mappers.DeviceMapper;
import com.r1chjames.iot.monitoring.entities.Device;
import org.jdbi.v3.sqlobject.customizer.Bind;
import org.jdbi.v3.sqlobject.customizer.BindBean;
import org.jdbi.v3.sqlobject.statement.SqlQuery;
import org.jdbi.v3.sqlobject.statement.SqlUpdate;
import org.jdbi.v3.sqlobject.statement.UseRowMapper;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.UUID;

@Component
public interface DeviceDao {

    String selectFields = "d.id, d.name, d.type, d.ip, d.monitor_type, d.last_updated";
    String updateFields = "d.name, d.type, d.ip, d.monitor_type ";
    String updateValues = ":device.name, :device.type, :device.ip, :device.monitorType ";
    String patchUpdateValues = "d.name = COALESCE(:device.name, name), d.type = COALESCE(:device.type, type), d.ip = COALESCE(:device.ip, ip), d.monitor_type = COALESCE(:device.monitorType, monitor_type) ";
    String tableName = "devices d";

    @UseRowMapper(DeviceMapper.class)
    @SqlQuery(
            "SELECT " +
                    selectFields +
                    " FROM "
                    + tableName)
    List<Device> getAllDevices();

    @UseRowMapper(DeviceMapper.class)
    @SqlQuery(
            "SELECT " +
                    selectFields +
                    " FROM "
                    + tableName +
                    "WHERE " +
                    "id = :id")
    Device getDeviceById(@Bind("id") UUID id);

    @UseRowMapper(DeviceMapper.class)
    @SqlQuery(
            "SELECT " +
                    selectFields +
                    " FROM "
                    + tableName +
                    "WHERE " +
                    "d.ip = :ip")
    Device getDeviceByIp(@Bind("ip") String ip);

    @UseRowMapper(DeviceMapper.class)
    @SqlQuery(
            "INSERT INTO " +
                    tableName +
                    "(" + updateFields + ")" +
                    "VALUES (" +
                    updateValues +
                    ") RETURNING *")
    Device addDevice(@BindBean("device") Device device);

    @UseRowMapper(DeviceMapper.class)
    @SqlQuery(
            "UPDATE " +
                    tableName +
                    "SET " +
                    patchUpdateValues +
                    "WHERE d.id = :id " +
                    "RETURNING *")
    Device updateDevice(@Bind("id") UUID id, @BindBean("device") Device device);

    @SqlUpdate(
            "DELETE FROM " +
                    tableName +
                    "WHERE d.id = :id ")
    void deleteDevice(@Bind("id") UUID id);
}
