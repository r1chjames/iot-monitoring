package com.r1chjames.iot.monitoring.dao;

import com.r1chjames.iot.monitoring.dao.mappers.TaskMapper;
import com.r1chjames.iot.monitoring.entities.Task;
import org.jdbi.v3.sqlobject.statement.SqlQuery;
import org.jdbi.v3.sqlobject.statement.UseRowMapper;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public interface TaskDao {

    String selectFields = "st.id, d.id, d.name, d.type, d.ip, d.monitor_type, d.last_updated, st.cron_expression, st.last_updated";
    String tableName = "scheduled_tasks st";
    String devicesTableName = "devices d ";

    @UseRowMapper(TaskMapper.class)
    @SqlQuery(
            "SELECT " +
                    selectFields +
                    " FROM "
                    + tableName +
                    "JOIN " +
                    devicesTableName +
                    "ON st.device_id = d.id")
    List<Task> getAllTasks();

}
