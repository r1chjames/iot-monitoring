package com.r1chjames.iot.monitoring.controllers;

import com.r1chjames.iot.monitoring.entities.Device;
import com.r1chjames.iot.monitoring.services.DeviceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

@RestController
@RequestMapping("/devices")
public class DeviceController {

    @Autowired
    private DeviceService deviceService;

    @RequestMapping(value = "/id/{id}", method = RequestMethod.GET)
    public ResponseEntity<?> getDeviceInfoById(@PathVariable("id") UUID id) {
        Device device = deviceService.getDeviceById(id);
        return new ResponseEntity<>(device, HttpStatus.OK);
    }

    @RequestMapping(value = "/ip/{ip}", method = RequestMethod.GET)
    public ResponseEntity<?> getDeviceInfoByIp(@PathVariable("ip") String ip) {
        Device device = deviceService.getDeviceByIp(ip);
        return new ResponseEntity<>(device, HttpStatus.OK);
    }

    @RequestMapping(value = "/", method = RequestMethod.POST)
    public ResponseEntity<?> addDevice(@RequestBody Device device) {
        Device deviceInserted = deviceService.addDevice(device);
        return new ResponseEntity<>(deviceInserted, HttpStatus.ACCEPTED);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    public ResponseEntity<?> fullUpdateDevice(@PathVariable("id") UUID id, @RequestBody Device device) {
        Device deviceUpdated = deviceService.fullUpdateDevice(id, device);
        return new ResponseEntity<>(deviceUpdated, HttpStatus.ACCEPTED);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PATCH)
    public ResponseEntity<?> partialUpdateDevice(@PathVariable("id") UUID id, @RequestBody Device device) {
        Device deviceUpdated = deviceService.partialUdateDevice(id, device);
        return new ResponseEntity<>(deviceUpdated, HttpStatus.ACCEPTED);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<?> deleteDevice(@PathVariable("id") UUID id) {
        deviceService.daleteDevice(id);
        return new ResponseEntity<>(HttpStatus.ACCEPTED);
    }
}