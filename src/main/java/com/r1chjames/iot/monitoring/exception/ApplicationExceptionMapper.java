package com.r1chjames.iot.monitoring.exception;

import lombok.extern.slf4j.Slf4j;

import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import java.util.Collections;
import java.util.List;

import static com.r1chjames.iot.monitoring.exception.GeneralExceptionMapper.API_INDEX;
import static com.r1chjames.iot.monitoring.utils.JsonUtils.toJson;
import static org.apache.commons.lang3.StringUtils.leftPad;

@Slf4j
public class ApplicationExceptionMapper implements ExceptionMapper<ApplicationException> {

    @Context
    private HttpHeaders headers;

    private List<Integer> codesToNotLogAsError = Collections.singletonList(404);

    @Override
    public Response toResponse(ApplicationException e) {
        Integer returnCode = e.getErrorSequences().getReturnCode();
        String errorCode = String.format("%s.%s.%s",
                leftPad(String.valueOf(returnCode), 3, '0'),
                leftPad(String.valueOf(API_INDEX), 2, '0'));

        String errorMessage = e.getErrorSequences().getMessage();

        ApiErrorResponse errorResponse = new ApiErrorResponse(returnCode, errorCode, errorMessage);

        if (codesToNotLogAsError.contains(e.getErrorSequences().getReturnCode())) {
            log.info("EX: {}, caused by: ", toJson(errorResponse), e.getCause());
        } else {
            log.error("EX: {}, caused by: ", toJson(errorResponse), e.getCause());
        }

        Response.ResponseBuilder responseBuilder = Response
                .status(returnCode)
                .entity(errorResponse)
                .type(headers.getMediaType());

        return responseBuilder.build();
    }
}
