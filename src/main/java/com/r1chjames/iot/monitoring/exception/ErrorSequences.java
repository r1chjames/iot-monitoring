package com.r1chjames.iot.monitoring.exception;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum ErrorSequences {

    NOT_FOUND("Resource not found", 404),
    AN_UNEXPECTED_ERROR_HAS_OCCURRED("", 500),
    JSON_PARSING_ERROR("Failure parsing JSON", 400),
    JSON_PARSING_ERROR_RETURNING_EMPTY("Failure parsing JSON, returning empty object", 400);


    private final String message;
    private final Integer returnCode;
}
