package com.r1chjames.iot.monitoring.exception;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ApiErrorResponse {
    private Integer httpResponseCode;
    private String errorCode;
    private String errorMessage;
}
