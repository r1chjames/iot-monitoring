package com.r1chjames.iot.monitoring.exception;

import lombok.Getter;

@Getter
public class ApplicationException extends RuntimeException {
    ErrorSequences errorSequences;

    public ApplicationException(ErrorSequences errorSequences) {
        super(errorSequences.getMessage());
        this.errorSequences = errorSequences;
    }

    public ApplicationException(Throwable cause, ErrorSequences errorSequences) {
        super(errorSequences.getMessage(), cause);
        this.errorSequences = errorSequences;
    }
}
