//package utils;
//
//
//import com.morrisons.loyalty.points.config.ApplicationConfiguration;
//import com.morrisons.loyalty.points.config.JdbiProviderModule;
//import com.opentable.db.postgres.embedded.EmbeddedPostgres;
//import io.dropwizard.db.DataSourceFactory;
//import org.flywaydb.core.Flyway;
//import org.jdbi.v3.core.Jdbi;
//import org.skife.jdbi.v2.DBI;
//import org.skife.jdbi.v2.Handle;
//import org.springframework.jdbc.datasource.embedded.DataSourceFactory;
//import ru.vyarus.dropwizard.guice.test.GuiceyAppRule;
//
//import java.io.IOException;
//import java.util.List;
//
//public class EmbeddedPostgresDB {
//
//    private EmbeddedPostgres db;
//
//    public void stop() throws IOException {
//        db.close();
//    }
//
//    public void startDatabase(DataSourceFactory database) throws IOException {
//        int port = Integer.parseInt(database.getUrl().substring(database.getUrl().lastIndexOf(':') + 1, database.getUrl().lastIndexOf('/')));
//        db = EmbeddedPostgres.builder().setPort(port).start();
//        database.setPassword("postgres");
//        database.setUser("postgres");
//        database.setUrl(String.format("jdbc:postgresql://localhost:%s/postgres", db.getPort()));
//    }
//
//    public void setUpDatabase(String filesLocation, DataSourceFactory database) {
//        Flyway flyway = new Flyway();
//        flyway.setDataSource(database.getUrl(), database.getUser(), database.getPassword());
//        flyway.setLocations(filesLocation);
//        flyway.db();
//    }
//
//    public void truncateTables(List<String> tableToClear) {
//        Jdbi dbi = new JdbiProviderModule().prepareJdbi(rule.getEnvironment(), rule.getConfiguration());
//        truncateTables(tableToClear, dbi);
//    }
//
//    public void truncateTables(List<String> tableToClear, DBI dbi) {
//        tableToClear.forEach(t -> truncateTable(dbi, t));
//    }
//
//    private void truncateTable(DBI dbi, String tableName) {
//        Handle handle = dbi.open();
//        handle.begin();
//        handle.execute(String.format("TRUNCATE TABLE %s", tableName));
//        handle.commit();
//        handle.close();
//    }
//}
