package com.r1chjames.iot.monitoring.controllers;

import com.r1chjames.iot.monitoring.entities.Device;
import com.r1chjames.iot.monitoring.services.DeviceService;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.UUID;

import static com.r1chjames.iot.monitoring.utils.JsonUtils.readFile;
import static com.r1chjames.iot.monitoring.utils.JsonUtils.readJsonFile;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doReturn;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static utils.JunitUtils.flattenString;
import static utils.JunitUtils.jacksonDateTimeConverter;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class DeviceControllerTest {

    @Rule
    public MockitoRule rule = MockitoJUnit.rule();

    @Autowired
    private MockMvc mockMvc;

    @Mock
    private DeviceService deviceService;

    @InjectMocks
    private DeviceController deviceController;

    @Before
    public void init(){
        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders
                .standaloneSetup(deviceController)
                .setMessageConverters(jacksonDateTimeConverter())
                .build();
    }

    @Test
    public void getDeviceById() throws Exception {
        String deviceId = "1743faae-6092-4967-ba20-4dd9376312f2";
        String pathToJson = "src/test/data/json/device.json";

        doReturn(readJsonFile(pathToJson, Device.class)).when(deviceService).getDeviceById(UUID.fromString(deviceId));

        mockMvc.perform(MockMvcRequestBuilders.get("/devices/id/" + deviceId).accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().string(flattenString(readFile(pathToJson))));
    }

    @Test
    public void getDeviceByIp() throws Exception {
        String deviceIp = "10.0.0.0";
        String pathToJson = "src/test/data/json/device.json";

        doReturn(readJsonFile(pathToJson, Device.class)).when(deviceService).getDeviceByIp(anyString());

        mockMvc.perform(MockMvcRequestBuilders.get("/devices/ip/" + deviceIp).accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().string(flattenString(readFile(pathToJson))));
    }

    @Test
    public void addDevice() throws Exception {
        Device device = readJsonFile("src/test/data/json/device.json", Device.class);
        String deviceString = readFile("src/test/data/json/device.json");

        doReturn(device).when(deviceService).addDevice(device);

        mockMvc.perform(MockMvcRequestBuilders.post("/devices/").content(deviceString).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isAccepted());
    }

    @Test
    public void fullUpdateDevice() throws Exception {
        Device device = readJsonFile("src/test/data/json/device.json", Device.class);
        String deviceId = "1743faae-6092-4967-ba20-4dd9376312f2";
        String deviceString = readFile("src/test/data/json/device.json");
        String fullDeviceString = readFile("src/test/data/json/device.json");

        doReturn(device).when(deviceService).fullUpdateDevice(any(), any());

        mockMvc.perform(MockMvcRequestBuilders.put("/devices/" + deviceId).content(deviceString).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isAccepted())
                .andExpect(content().string(flattenString(fullDeviceString)));
    }

    @Test
    public void partialUpdateDevice() throws Exception {
        Device fullDevice = readJsonFile("src/test/data/json/device.json", Device.class);
        String deviceId = "1743faae-6092-4967-ba20-4dd9376312f2";
        String partialDeviceString = readFile("src/test/data/json/partial_device.json");
        String fullDeviceString = readFile("src/test/data/json/device.json");

        doReturn(fullDevice).when(deviceService).partialUdateDevice(any(), any());

        mockMvc.perform(MockMvcRequestBuilders.patch("/devices/" + deviceId).content(partialDeviceString).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isAccepted())
                .andExpect(content().string(flattenString(fullDeviceString)));
    }

    @Test
    public void deleteDevice() throws Exception {
        String deviceId = "1743faae-6092-4967-ba20-4dd9376312f2";

        doNothing().when(deviceService).daleteDevice(any());

        mockMvc.perform(MockMvcRequestBuilders.delete("/devices/" + deviceId).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isAccepted());
    }

}